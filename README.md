# Introduction

This documentation explains the basic configuration in order to start working with your own GitLab groups and projects.

Please refer to https://github.com/sourcegraph/sourcegraph and https://docs.sourcegraph.com/ for full documentation.

# Requirements

**Prerequisite: http://configtraining.web.cern.ch/configtraining/introduction/permissions.html#normal-access**

* Access to an [Openstack tenant/project](http://clouddocs.web.cern.ch/clouddocs/projects/creating_projects.html)
* Enough [quota](http://clouddocs.web.cern.ch/clouddocs/details/quotas.html) to deploy a new node. 
* A volume is recommended if you expect your data to grow considerabily, as the node's disk is limited.
    * (Optional) In order to improve performance, use an [`io1`](http://clouddocs.web.cern.ch/clouddocs/details/block_volumes.html) volume type. You might need to request a [quota change](http://clouddocs.web.cern.ch/clouddocs/projects/project_quota_request.html).
* A service account and its [Gitlab Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html).
* Register your instance on https://sso-management.web.cern.ch/SSO/RegisterApplication.aspx. 
    * Default Shibboleth Classic/SAML1.1.
    * Application Uri: https://<YOUR_NODE_NAME>.cern.ch/Shibboleth.sso/ADFS
    * Application Homepage: https://<YOUR_NODE_NAME>.cern.ch

# Initial deployment

You will need to add the [Sourcegraph module](https://gitlab.cern.ch/ai/it-puppet-module-cern_sourcegraph) to your hostgroup, for example: 

```
class hg_your_hostgroup (
    String $docker_image_version = 'sourcegraph/server:2.13.5',
    String $sourcegraph_config_path = '/sourcegraph/config',
    String $sourcegraph_data_path = '/sourcegraph/data',
    String $authorized_egroups = 'it-service-vcs ACC-coperadev',
) {
    class { '::cern_sourcegraph':
        docker_image_version    => $docker_image_version,
        sourcegraph_config_path => $sourcegraph_config_path,
        sourcegraph_data_path   => $sourcegraph_data_path,
        authorized_egroups      => $authorized_egroups,
    }
}
```

Once your hostgroup is configured:

```bash
# Access to aiadm to create a new puppet-managed node
ssh aiadm
# Set your Openstack tenant
eval `ai-rc "<YOUR_TENANT>"`
# Create the new node. This example uses an io1 volume of 250GB, change as necessary
ai-bs --landb-mainuser <YOUR_RESPONSIBLE_EGROUP_OR_USER> --landb-responsible <YOUR_RESPONSIBLE_EGROUP_OR_USER> \
  --nova-flavor m2.large --cc7 -g <YOUR_HOSTGROUP> --foreman-environment qa \
  --nova-attach-new-volume vdb=250GB:delete-on-terminate:type=io1 <YOUR_NODE_NAME>.cern.ch
```

You now have to wait for Puppet to run on this node. Once it finishes you will be able to use `ssh root@<YOUR_NODE_NAME>`.

(Optional) If you want to use your [volume](http://clouddocs.web.cern.ch/clouddocs/details/block_volumes.html) to store your Sourcegraph data, format and mount it, as this directory will be used by the puppet module:
```bash
ssh root@<YOUR_NODE_NAME>
mkdir /sourcegraph
# Format the new volume
mkfs -t ext4 -L sourcegraphvol /dev/vdb
# Mount the volume on the Sourcegraph directory
mount -L testvol /sourcegraph
```

Using a volume is recommended if the git repositories start growing above the VM hard drive. `io1` type is recommended as offers the best I/O ratio.

# Instance configuration

On first access you will be prompted to set up an administration account.

Sourcegraph cannot run on a Kubernetes cluster as it requires an enterprise licence.

Sourcegraph free version needs to run as a Docker container, needing side-containers for Code-Intelligence. The recommended deployment requires a puppet-managed node.

Sourcegraph requires a [Gitlab Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) with access to the Groups/Projects you want to integrate on this tool. A [service account](https://account.cern.ch/account/Help/?kbid=011010) is recommended. Please see [Allow Sourcegraph access to projects](#allow-sourcegraph-access-to-projects) section for further information.

Configure the instance following this skeleton (`/sourcegraph/config/sourcegraph-config.json`) or directly through `https://<YOUR_NODE_NAME>.cern.ch/site-admin`:
```
{
  "auth.providers": [
    {
      "type": "builtin"
    }
  ],
  "auth.public": true,
  "disablePublicRepoRedirects": true,
  "maxReposToSearch": -1,
  "gitlab": [
    {
      "url": "https://gitlab.cern.ch",
      "token": "<YOUR_SERVICE_ACCOUNT_ACCESS_TOKEN>",
      initialRepositoryEnablement: true
    }
  ]
}
```

After Sourcegraph fetches all the repositories the configured Access token has access to, detected repositories will be mirrored into Sourcegraph **and you can start searching on it**.

# Authentication

Sourcegraph free version does not support SSO integration, meaning that users will be local to the instance. You can create accounts for your users.

You can configure Sourcegraph to [allow signing up](https://docs.sourcegraph.com/admin/site_config/all#allowsignup-boolean) or [allow search without an account](https://docs.sourcegraph.com/admin/site_config/all#auth-public-boolean), but **this configuration will make your code visible to anyone with access to you instance**. [Sourcegraph module](https://gitlab.cern.ch/ai/it-puppet-module-cern_sourcegraph) is configured to use a SSO proxy so only the desired egroups can access it while everyone with access can search without login in.

After initial installation you will be asked for a local administration account.

# Allow Sourcegraph access to projects

In order to make Sourcegraph mirror your projects, the service account corresponding to the configured Access token needs to be a member of them.
This can be done either adding the account as a member of your projects or as a member of the group owning your projects.

Keep in mind that if your project or group membership is managed through LDAP egroups, the service account would need to be a member of these egroups.

If this service account is removed from the members, the corresponding projects will no longer be mirrored so new content will not be updated.

# Integration with Gitlab

Sourcegraph has a CERN SSO proxy in front of it to allow users to search code without using local accounts, this means that **browser extensions will not work** with `gitlab.cern.ch`.
This might change in the future, see https://gitlab.com/gitlab-org/gitlab-ce/issues/41925